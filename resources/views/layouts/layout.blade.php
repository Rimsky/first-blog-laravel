<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/app.css') }}" />
    <link href="https://fonts.googleapis.com/css?family=Lato&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Press+Start+2P&display=swap" rel="stylesheet">
    <title>Virtual BJ</title>
</head>

<body>
    <header class="header">
        <a class="header__logo" href="/">VirtualBJ</a>
        <div class="nav">
            <a  href="/" >HOME</a>
            <a  href="/news" >NEWS</a>
            <a  href="/games">OUR GAMES</a>
            <a  href="/about">ABOUT US</a>
            <a  href="/contacts">CONTACTS</a>
        </div>
        @if(Auth::check())
            <div class="log_panel">
                <a href="/admin">Admin</a>
                <br>
                <a href="/logout">Logout</a>
                <br>
            </div>
        @endif
    </header>

    <div class="page">
    <main class="main">
        @yield('content')
    </main>

    <footer>
        <div class="footer__discription"> Какой-то там текст.</div>
        <div class="footer__copyright">© 2020 - VirtualBJ. All Rights Reserved. Designed & Developed by VirtualBJ Team</div>
    </footer>
</div>
</body>
</html>
